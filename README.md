# BookerTrans2

用于 iBooker/ApacheCN 翻译项目的 HTML 百度翻译工具

## 安装

通过pip（推荐）：

```
pip install BookerTrans2
```

从源码安装：

```
pip install git+https://github.com/apachecn/BookerTrans
```

## 使用说明

```
btrans [-h] [-v] [-P PROXY] [-t TIMEOUT] [-w WAIT_SEC] 
       [-r RETRY] [-s SRC] [-d DST] <-i APP_ID> <-k APP_KEY>
       fname
       
-P PROXY: 代理，格式为 \d+\.\d+\.\d+\.\d+:\d+，默认为空
-t TIMEOUT: 超时时间，以秒为单位，默认为 8
-w WAIT_SEC: 两次翻译之间的延迟（以秒为单位），默认为 0.1
-r RETRY: 重试次数，默认为 10
-s SRC: 源语言，默认为 auto
-d DST: 目标语言，默认为 zh
-i APP_ID: 开放平台 APP ID，默认为系统变量 $BAIDU_FANYI_APPID
-k APP_KEY: 开放平台 APP KEY，默认为系统变量 $BAIDU_FANYI_APPKEY
fname: HTML 文件名称，或者文件所在的目录名称
```

## 协议

本项目基于 SATA 协议发布。

您有义务为此开源项目点赞，并考虑额外给予作者适当的奖励。

## 赞助我们

![](https://home.apachecn.org/img/about/donate.jpg)

## 另见

+   [ApacheCN 学习资源](https://docs.apachecn.org/)
+   [计算机电子书](http://it-ebooks.flygon.net)
+   [布客新知](http://flygon.net/ixinzhi/)
